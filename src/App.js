import React from 'react'

// components
import Header from './components/Header'
import Routes from './routes'

function App() {
  return (
    <div className="wrap">
      <div className="content-page">
        <Header />
        <div className="content">
          <div className="container-fluid">
            <Routes />
          </div>
        </div>
      </div>
    </div >
  )
}

export default App