import React from 'react'

// @material-ui/core components
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid'
import Card from '@material-ui/core/Card'
import Button from '@material-ui/core/Button'
import Table from '@material-ui/core/Table'
import TableHead from '@material-ui/core/TableHead'
import TableBody from '@material-ui/core/TableBody'
import TableFooter from '@material-ui/core/TableFooter'
import TableRow from '@material-ui/core/TableRow'
import TableCell from '@material-ui/core/TableCell'
import IconButton from '@material-ui/core/IconButton'
import Avatar from '@material-ui/core/Avatar'
import Tooltip from '@material-ui/core/Tooltip'
import TablePagination from '@material-ui/core/TablePagination'

// components
import TablePaginationActions from './TablePaginationActions'

const list = [
    { id: 54, role: "Teacher", section: "section2", firstname: "Hayward", lastname: "Billo", email: "haywardbillo@gmail.com", contactNumber: "0846548178", status: true },
    { id: 75, role: "Teacher", section: "section2", firstname: "Adler", lastname: "Villa", email: "adlervilla@gmail.com", contactNumber: "0846548178", status: false },
    { id: 45, role: "Teacher", section: "section2", firstname: "Janna", lastname: "Gardner", email: "jannamacFayden@gmail.com", contactNumber: "0846548178", status: true },
    { id: 86, role: "Teacher", section: "section2", firstname: "Patel", lastname: "Lane", email: "peradi7327@4tmail.net", contactNumber: "0846548178", status: false },
    { id: 78, role: "Teacher", section: "section2", firstname: "Burns", lastname: "Gonzales", email: "wendycollins25@gmail.com", contactNumber: "0846548178", status: false },
    { id: 21, role: "Teacher", section: "section2", firstname: "Edwards", lastname: "Barnes", email: "wendycollins25@gmail.com", contactNumber: "0846548178", status: true },
    { id: 68, role: "Teacher", section: "section2", firstname: "Kelly", lastname: "Payne", email: "wendycollins25@gmail.com", contactNumber: "0846548178", status: true },
    { id: 45, role: "Teacher", section: "section2", firstname: "Ellis", lastname: "Johnston", email: "matthewscott58@gmail.com", contactNumber: "0846548178", status: true },
    { id: 86, role: "Teacher", section: "section2", firstname: "Gibson", lastname: "King", email: "haywardbillo@gmail.com", contactNumber: "0846548178", status: true },
    { id: 86, role: "Teacher", section: "section2", firstname: "Brown", lastname: "Sullivan", email: "gavinjohnson47@gmail.com", contactNumber: "0846548178", status: true },
    { id: 86, role: "Teacher", section: "section2", firstname: "Rodriguez", lastname: "Cruz", email: "timothyeady18@gmail.com", contactNumber: "0846548178", status: true },
    { id: 86, role: "Teacher", section: "section2", firstname: "Mills", lastname: "Arnold", email: "stellahill61@gmail.com", contactNumber: "0846548178", status: true },
    { id: 86, role: "Teacher", section: "section2", firstname: "Wood", lastname: "Richardson", email: "jeramiahvasko21@gmail.com", contactNumber: "0846548178", status: true },
    { id: 86, role: "Teacher", section: "section2", firstname: "Palmer", lastname: "Reed", email: "michaelgagnon84@gmail.com", contactNumber: "0846548178", status: true },
]

const ListTeacher = () => {

    const [page, setPage] = React.useState(0)
    const [rowsPerPage, setRowsPerPage] = React.useState(5)

    const handleChangePage = (event, newPage) => {
        setPage(newPage)
    }

    const handleChangeRowsPerPage = event => {
        setRowsPerPage(parseInt(event.target.value, 10))
        setPage(0)
    }

    return (
        <React.Fragment>
            <Typography variant="h6" style={{ padding: "15px 0px" }}>Teacher List</Typography>
            <Card
                style={{
                    color: "#fff",
                    border: "none",
                    boxShadow: "0 0 35px 0 rgba(154,161,171,.15)",
                    padding: "20px",
                    overflowX: "auto"
                }}
            >
                <Grid container>
                    <Grid container justify="flex-start">
                        <Button
                            component="a" href={"/teacher-add"}
                            startIcon={<i className="material-icons-outlined">add_circle_outline</i>}
                            variant="contained"
                            size="small"
                            style={{
                                background: "#0acf97",
                                border: 0,
                                borderColor: "#0acf97",
                                boxShadow: "0 2px 6px 0 rgba(10,207,151,.5)",
                                color: "#fff",
                                padding: "7px 15px",
                            }}
                        >

                            Add Teacher
                        </Button>
                    </Grid>
                    <Grid container alignContent="center">
                        <Table style={{ margin: "35px 0px 25px 0px", color: "#6c757d" }}>
                            <TableHead style={{ background: "#f1f3fa" }}>
                                <TableRow>
                                    <TableCell>#</TableCell>
                                    <TableCell align="left">Avatar</TableCell>
                                    <TableCell align="left">Role</TableCell>
                                    <TableCell align="left">Section</TableCell>
                                    <TableCell align="left">Firstname</TableCell>
                                    <TableCell align="center">Lastname</TableCell>
                                    <TableCell align="center">E-mail</TableCell>
                                    <TableCell align="center">Contact Number</TableCell>
                                    <TableCell align="center">Status</TableCell>
                                    <TableCell align="center">Action</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {(rowsPerPage > 0
                                    ? list.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                    : list
                                ).map((item) => (
                                    <TableRow key={item.firstname}>
                                        <TableCell>{item.id}</TableCell>
                                        <TableCell align="left">
                                            <Avatar>{item.firstname.charAt(0)}</Avatar>
                                        </TableCell>
                                        <TableCell align="left">{item.role}</TableCell>
                                        <TableCell align="left">{item.section}</TableCell>
                                        <TableCell align="left">{item.firstname}</TableCell>
                                        <TableCell align="left">{item.lastname}</TableCell>
                                        <TableCell align="left">{item.email}</TableCell>
                                        <TableCell align="left">{item.contactNumber}</TableCell>
                                        <TableCell align="left">
                                            {item.status ?
                                                <span className="badge badge-success">Active</span>
                                                :
                                                <span className="badge badge-danger">Deactivated</span>
                                            }
                                        </TableCell>
                                        <TableCell align="center">
                                            <Tooltip title="Edit" placement="top">
                                                <IconButton size="small">
                                                    <i className="material-icons-outlined md-18">edit</i>
                                                </IconButton>
                                            </Tooltip>
                                            <Tooltip title="Delete" placement="top">
                                                <IconButton size="small">
                                                    <i className="material-icons-outlined md-18">delete</i>
                                                </IconButton>
                                            </Tooltip>
                                        </TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                            <TableFooter>
                                <TableRow>
                                    <TablePagination
                                        rowsPerPageOptions={[5, 10, 25]}
                                        colSpan={10}
                                        count={list.length}
                                        rowsPerPage={rowsPerPage}
                                        page={page}
                                        onChangePage={handleChangePage}
                                        onChangeRowsPerPage={handleChangeRowsPerPage}
                                        ActionsComponent={TablePaginationActions}
                                    />
                                </TableRow>
                            </TableFooter>
                        </Table>
                    </Grid>
                </Grid>
            </Card>
        </React.Fragment>
    )
}

export default ListTeacher