import React from 'react'

// @material-ui/core components
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid'
import Card from '@material-ui/core/Card'
import Button from '@material-ui/core/Button'
import Table from '@material-ui/core/Table'
import TableHead from '@material-ui/core/TableHead'
import TableBody from '@material-ui/core/TableBody'
import TableFooter from '@material-ui/core/TableFooter'
import TableRow from '@material-ui/core/TableRow'
import TableCell from '@material-ui/core/TableCell'
import IconButton from '@material-ui/core/IconButton'
import Tooltip from '@material-ui/core/Tooltip'
import TablePagination from '@material-ui/core/TablePagination'

// components
import TablePaginationActions from './TablePaginationActions'

const list = [
    { id: 54, firstname: "Hayward", lastname: "Billo", email: "haywardbillo@gmail.com", status: true },
    { id: 75, firstname: "Adler", lastname: "Villa", email: "adlervilla@gmail.com", status: false },
    { id: 45, firstname: "Janna", lastname: "MacFayden", email: "jannamacFayden@gmail.com", status: true },
    { id: 86, firstname: "Hayward", lastname: "Billo", email: "haywardbillo@gmail.com", status: false },
    { id: 78, firstname: "Hayward", lastname: "Billo", email: "haywardbillo@gmail.com", status: false },
    { id: 21, firstname: "Hayward", lastname: "Billo", email: "haywardbillo@gmail.com", status: true },
    { id: 68, firstname: "Hayward", lastname: "Billo", email: "haywardbillo@gmail.com", status: true },
    { id: 45, firstname: "Hayward", lastname: "Billo", email: "haywardbillo@gmail.com", status: true },
    { id: 86, firstname: "Hayward", lastname: "Billo", email: "haywardbillo@gmail.com", status: true },
    { id: 86, firstname: "Hayward", lastname: "Billo", email: "haywardbillo@gmail.com", status: true },
    { id: 86, firstname: "Hayward", lastname: "Billo", email: "haywardbillo@gmail.com", status: true },
    { id: 86, firstname: "Hayward", lastname: "Billo", email: "haywardbillo@gmail.com", status: true },
    { id: 86, firstname: "Hayward", lastname: "Billo", email: "haywardbillo@gmail.com", status: true },
    { id: 86, firstname: "Hayward", lastname: "Billo", email: "haywardbillo@gmail.com", status: true },
]

const ListStaff = () => {

    const [page, setPage] = React.useState(0)
    const [rowsPerPage, setRowsPerPage] = React.useState(5)

    const handleChangePage = (event, newPage) => {
        setPage(newPage)
    }

    const handleChangeRowsPerPage = event => {
        setRowsPerPage(parseInt(event.target.value, 10))
        setPage(0)
    }

    return (
        <React.Fragment>
            <Typography variant="h6" style={{ padding: "15px 0px" }}>Staff List</Typography>
            <Card
                style={{
                    color: "#fff",
                    border: "none",
                    boxShadow: "0 0 35px 0 rgba(154,161,171,.15)",
                    padding: "20px",
                    overflowX: "auto"
                }}
            >
                <Grid container>
                    <Grid container justify="flex-start">
                        <Button
                            component="a" href={"/staff-add"}
                            startIcon={<i className="material-icons-outlined">add_circle_outline</i>}
                            variant="contained"
                            size="small"
                            style={{
                                background: "#0acf97",
                                border: 0,
                                borderColor: "#0acf97",
                                boxShadow: "0 2px 6px 0 rgba(10,207,151,.5)",
                                color: "#fff",
                                padding: "7px 15px",
                            }}
                        >

                            Add Staff
                        </Button>
                    </Grid>
                    <Grid container alignContent="center">
                        <Table style={{ margin: "35px 0px 25px 0px", color: "#6c757d" }}>
                            <TableHead style={{ background: "#f1f3fa" }}>
                                <TableRow>
                                    <TableCell>#</TableCell>
                                    <TableCell align="left">Firstname</TableCell>
                                    <TableCell align="left">Lastname</TableCell>
                                    <TableCell align="left">E-mail</TableCell>
                                    <TableCell align="left">Status</TableCell>
                                    <TableCell align="center">Action</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {(rowsPerPage > 0
                                    ? list.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                    : list
                                ).map((item) => (
                                    <TableRow key={item.firstname}>
                                        <TableCell>{item.id}</TableCell>
                                        <TableCell align="left">{item.firstname}</TableCell>
                                        <TableCell align="left">{item.lastname}</TableCell>
                                        <TableCell align="left">{item.email}</TableCell>
                                        <TableCell align="left">
                                            {item.status ?
                                                <span className="badge badge-success">Active</span>
                                                :
                                                <span className="badge badge-danger">Deactivated</span>
                                            }
                                        </TableCell>
                                        <TableCell align="center">
                                            <Tooltip title="Edit" placement="top">
                                                <IconButton size="small">
                                                    <i className="material-icons-outlined md-18">edit</i>
                                                </IconButton>
                                            </Tooltip>
                                            <Tooltip title="Delete" placement="top">
                                                <IconButton size="small">
                                                    <i className="material-icons-outlined md-18">delete</i>
                                                </IconButton>
                                            </Tooltip>
                                        </TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                            <TableFooter>
                                <TableRow>
                                    <TablePagination
                                        rowsPerPageOptions={[5, 10, 25]}
                                        colSpan={6}
                                        count={list.length}
                                        rowsPerPage={rowsPerPage}
                                        page={page}
                                        onChangePage={handleChangePage}
                                        onChangeRowsPerPage={handleChangeRowsPerPage}
                                        ActionsComponent={TablePaginationActions}
                                    />
                                </TableRow>
                            </TableFooter>
                        </Table>
                    </Grid>
                </Grid>
            </Card>
        </React.Fragment>
    )
}

export default ListStaff