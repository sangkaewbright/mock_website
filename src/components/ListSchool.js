import React from 'react'

// @material-ui/core components
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid'
import Card from '@material-ui/core/Card'
import Button from '@material-ui/core/Button'
import Avatar from '@material-ui/core/Avatar'
import Table from '@material-ui/core/Table'
import TableHead from '@material-ui/core/TableHead'
import TableBody from '@material-ui/core/TableBody'
import TableFooter from '@material-ui/core/TableFooter'
import TableRow from '@material-ui/core/TableRow'
import TableCell from '@material-ui/core/TableCell'
import IconButton from '@material-ui/core/IconButton'
import Tooltip from '@material-ui/core/Tooltip'
import TablePagination from '@material-ui/core/TablePagination'

// components
import TablePaginationActions from './TablePaginationActions'

const list = [
    { name: "School_A", logo: "", contactNumber: "0549654812", status: true },
    { name: "School_B", logo: "", contactNumber: "0549654812", status: false },
    { name: "School_C", logo: "", contactNumber: "0549654812", status: false },
    { name: "School_D", logo: "", contactNumber: "0549654812", status: false },
    { name: "School_F", logo: "", contactNumber: "0549654812", status: true },
    { name: "School_G", logo: "", contactNumber: "0549654812", status: true },
    { name: "School_H", logo: "", contactNumber: "0549654812", status: true },
    { name: "School_I", logo: "", contactNumber: "0549654812", status: false },
    { name: "School_J", logo: "", contactNumber: "0549654812", status: true },
    { name: "School_K", logo: "", contactNumber: "0549654812", status: true },
]

const ListSchool = () => {

    const [page, setPage] = React.useState(0)
    const [rowsPerPage, setRowsPerPage] = React.useState(5)

    const handleChangePage = (event, newPage) => {
        setPage(newPage)
    }

    const handleChangeRowsPerPage = event => {
        setRowsPerPage(parseInt(event.target.value, 10))
        setPage(0)
    }

    return (
        <React.Fragment>
            <Typography variant="h6" style={{ padding: "15px 0px" }}>School List</Typography>
            <Card
                style={{
                    color: "#fff",
                    border: "none",
                    boxShadow: "0 0 35px 0 rgba(154,161,171,.15)",
                    padding: "20px",
                    overflowX: "auto"
                }}
            >
                <Grid container>
                    <Grid container justify="flex-start">
                        <Button
                            component="a" href={"/school-add"}
                            startIcon={<i className="material-icons-outlined">add_circle_outline</i>}
                            variant="contained"
                            size="small"
                            style={{
                                background: "#0acf97",
                                border: 0,
                                borderColor: "#0acf97",
                                boxShadow: "0 2px 6px 0 rgba(10,207,151,.5)",
                                color: "#fff",
                                padding: "7px 15px",
                            }}
                        >

                            Add School
                        </Button>
                    </Grid>
                    <Grid container alignContent="center">
                        <Table style={{ margin: "35px 0px 25px 0px", color: "#6c757d" }}>
                            <TableHead style={{ background: "#f1f3fa" }}>
                                <TableRow>
                                    <TableCell>Name</TableCell>
                                    <TableCell align="left">Logo</TableCell>
                                    <TableCell align="left">Contact Number</TableCell>
                                    <TableCell align="left">Status</TableCell>
                                    <TableCell align="center">Action</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {(rowsPerPage > 0
                                    ? list.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                    : list
                                ).map((item) => (
                                    <TableRow key={item.name}>
                                        <TableCell>{item.name}</TableCell>
                                        <TableCell align="left">
                                            <Avatar style={{ margin: "10" }} src="logo.png" />
                                        </TableCell>
                                        <TableCell align="left">{item.contactNumber}</TableCell>
                                        <TableCell align="left">
                                            {item.status ?
                                                <span className="badge badge-success">Active</span>
                                                :
                                                <span className="badge badge-danger">Deactivated</span>
                                            }
                                        </TableCell>
                                        <TableCell align="center">
                                            <Tooltip title="Edit" placement="top">
                                                <IconButton size="small">
                                                    <i className="material-icons-outlined md-18">edit</i>
                                                </IconButton>
                                            </Tooltip>
                                            <Tooltip title="Delete" placement="top">
                                                <IconButton size="small">
                                                    <i className="material-icons-outlined md-18">delete</i>
                                                </IconButton>
                                            </Tooltip>
                                        </TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                            <TableFooter>
                                <TableRow>
                                    <TablePagination
                                        rowsPerPageOptions={[5, 10, 25]}
                                        colSpan={5}
                                        count={list.length}
                                        rowsPerPage={rowsPerPage}
                                        page={page}
                                        onChangePage={handleChangePage}
                                        onChangeRowsPerPage={handleChangeRowsPerPage}
                                        ActionsComponent={TablePaginationActions}
                                    />
                                </TableRow>
                            </TableFooter>
                        </Table>
                    </Grid>
                </Grid>
            </Card>
        </React.Fragment>
    )
}

export default ListSchool