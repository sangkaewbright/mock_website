import React from 'react'

// @material-ui/core components=
import Typography from '@material-ui/core/Typography'
import Card from '@material-ui/core/Card'
import Grid from '@material-ui/core/Grid'
import FormControl from '@material-ui/core/FormControl'
import InputLabel from '@material-ui/core/InputLabel'
import OutlinedInput from '@material-ui/core/OutlinedInput'
import Button from '@material-ui/core/Button'

const AddSchoolBox = () => {

    return (
        <React.Fragment>
            <Typography variant="h6" style={{ padding: "15px 0px" }}>Add School</Typography>
            <Card
                style={{
                    color: "#6c757d",
                    border: "none",
                    boxShadow: "0 0 35px 0 rgba(154,161,171,.15)",
                    padding: "20px"
                }}
            >
                <Typography variant="h4" style={{ padding: "20px 0px" }}>School Info</Typography>

                <Grid container spacing={2}>
                    <Grid item xs={12} md={6}>
                        <FormControl variant="outlined" fullWidth margin="dense">
                            <InputLabel>Name</InputLabel>
                            <OutlinedInput
                                type={"text"}
                                autoFocus={true}
                                labelWidth={45} />
                        </FormControl>
                    </Grid>

                    <Grid item xs={12} md={6}>
                        <FormControl variant="outlined" fullWidth margin="dense">
                            <InputLabel>Contact Number</InputLabel>
                            <OutlinedInput
                                type={"text"}
                                labelWidth={120} />
                        </FormControl>
                    </Grid>

                    <Grid item xs={12} md={12}>
                        <input
                            accept="image/*"
                            style={{ display: "none" }}
                            id="text-button-file"
                            type="file"
                        />
                        <label htmlFor="text-button-file">
                            <Button
                                startIcon={<i className="material-icons-outlined">cloud_upload</i>}
                                component="span"
                                size="small"
                                style={{
                                    backgroundColor: "#f9375e",
                                    border: 0,
                                    borderColor: "#f82b54",
                                    boxShadow: "0 2px 6px 0 rgba(250,92,124,.5)",
                                    color: "#fff",
                                    padding: "7px 15px",
                                }}
                            >
                                Upload Logo
                            </Button>
                        </label>
                    </Grid>

                    <Grid
                        container
                        direction="row"
                        justify="flex-end"
                        alignItems="center"
                        style={{ padding: "10px 10px" }}
                    >
                        <Button
                            variant="contained"
                            size="small"
                            style={{
                                background: "#0acf97",
                                border: 0,
                                borderColor: "#0acf97",
                                boxShadow: "0 2px 6px 0 rgba(10,207,151,.5)",
                                color: "#fff",
                                padding: "7px 15px",
                            }}
                        >
                            Create School
                        </Button>
                    </Grid>
                </Grid>
            </Card>
        </React.Fragment>
    )
}

export default AddSchoolBox