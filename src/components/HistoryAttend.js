import React from 'react'

// @material-ui/core components
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid'
import Card from '@material-ui/core/Card'
import Table from '@material-ui/core/Table'
import TableHead from '@material-ui/core/TableHead'
import TableBody from '@material-ui/core/TableBody'
import TableFooter from '@material-ui/core/TableFooter'
import TableRow from '@material-ui/core/TableRow'
import TableCell from '@material-ui/core/TableCell'
import IconButton from '@material-ui/core/IconButton'
import Tooltip from '@material-ui/core/Tooltip'
import TablePagination from '@material-ui/core/TablePagination'

// components
import TablePaginationActions from './TablePaginationActions'

const list = [
    { dateTime: "2019-11-24", room: 548, grade: 3.50, attend: "2019-11-24", absent: 5, total: 9, updated: "2019-11-24" },
    { dateTime: "2019-11-24", room: 450, grade: 1.50, attend: "2019-11-24", absent: 5, total: 10, updated: "2019-11-24" },
    { dateTime: "2019-11-24", room: 302, grade: 3.25, attend: "2019-11-24", absent: 5, total: 10, updated: "2019-11-24" },
    { dateTime: "2019-11-24", room: 301, grade: 2.20, attend: "2019-11-24", absent: 5, total: 5, updated: "2019-11-24" },
    { dateTime: "2019-11-24", room: 302, grade: 3.25, attend: "2019-11-24", absent: 5, total: 10, updated: "2019-11-24" },
    { dateTime: "2019-11-24", room: 450, grade: 1.50, attend: "2019-11-24", absent: 5, total: 10, updated: "2019-11-24" },
    { dateTime: "2019-11-24", room: 548, grade: 3.50, attend: "2019-11-24", absent: 5, total: 9, updated: "2019-11-24" },
]

const HistoryAttend = () => {

    const [page, setPage] = React.useState(0)
    const [rowsPerPage, setRowsPerPage] = React.useState(5)

    const handleChangePage = (event, newPage) => {
        setPage(newPage)
    }

    const handleChangeRowsPerPage = event => {
        setRowsPerPage(parseInt(event.target.value, 10))
        setPage(0)
    }

    return (
        <React.Fragment>
            <Typography variant="h6" style={{ padding: "15px 0px" }}>Attend History</Typography>
            <Card
                style={{
                    color: "#fff",
                    border: "none",
                    boxShadow: "0 0 35px 0 rgba(154,161,171,.15)",
                    padding: "20px",
                    overflowX: "auto"
                }}
            >
                <Grid container>
                    <Grid container alignContent="center">
                        <Table style={{ margin: "35px 0px 25px 0px", color: "#6c757d" }}>
                            <TableHead style={{ background: "#f1f3fa" }}>
                                <TableRow>
                                    <TableCell>DateTime</TableCell>
                                    <TableCell align="left">Room</TableCell>
                                    <TableCell align="left">Grade</TableCell>
                                    <TableCell align="left">Attend</TableCell>
                                    <TableCell align="left">Absent</TableCell>
                                    <TableCell align="left">Total</TableCell>
                                    <TableCell align="left">Updated</TableCell>
                                    <TableCell align="center">Action</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {(rowsPerPage > 0
                                    ? list.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                    : list
                                ).map((item) => (
                                    <TableRow key={item.name}>
                                        <TableCell>{item.dateTime}</TableCell>
                                        <TableCell align="left">{item.room}</TableCell>
                                        <TableCell align="left">{item.grade}</TableCell>
                                        <TableCell align="left">{item.attend}</TableCell>
                                        <TableCell align="left">{item.absent}</TableCell>
                                        <TableCell align="left">{item.total}</TableCell>
                                        <TableCell align="left">{item.updated}</TableCell>
                                        <TableCell align="center">
                                            <Tooltip title="Edit" placement="top">
                                                <IconButton size="small" component="a" href={"/attend-update"}>
                                                    <i className="material-icons-outlined md-18">edit</i>
                                                </IconButton>
                                            </Tooltip>
                                            <Tooltip title="Delete" placement="top">
                                                <IconButton size="small">
                                                    <i className="material-icons-outlined md-18">delete</i>
                                                </IconButton>
                                            </Tooltip>
                                        </TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                            <TableFooter>
                                <TableRow>
                                    <TablePagination
                                        rowsPerPageOptions={[5, 10, 25]}
                                        colSpan={8}
                                        count={list.length}
                                        rowsPerPage={rowsPerPage}
                                        page={page}
                                        onChangePage={handleChangePage}
                                        onChangeRowsPerPage={handleChangeRowsPerPage}
                                        ActionsComponent={TablePaginationActions}
                                    />
                                </TableRow>
                            </TableFooter>
                        </Table>
                    </Grid>
                </Grid>
            </Card>
        </React.Fragment>
    )
}

export default HistoryAttend