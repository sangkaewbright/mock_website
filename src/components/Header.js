import React from 'react'

// @material-ui/core components
import AppBar from "@material-ui/core/AppBar"
import Toolbar from "@material-ui/core/Toolbar"
import IconButton from "@material-ui/core/IconButton"
import Hidden from "@material-ui/core/Hidden"
import Drawer from '@material-ui/core/Drawer'
import SwipeableDrawer from "@material-ui/core/Drawer"
import Switch from '@material-ui/core/Switch'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import FormGroup from '@material-ui/core/FormGroup'

// components
import ListSideMenu from './ListSideMenu'

const Header = () => {

    const [mobileOpen, setMobileOpen] = React.useState(false)
    const [login, setLogin] = React.useState(true)

    const handleSwitchToggle = event => {
        setLogin(event.target.checked)
    }

    const handleDrawerToggle = () => {
        setMobileOpen(!mobileOpen)
    }

    return (
        <div style={{ display: "flex" }}>
            <AppBar style={{
                background: "#fff",
                display: "flex",
                flexWrap: "wrap",
                boxShadow: "0 0 35px 0 rgba(154,161,171,.15)",
                position: "relative",
                justifyContent: "space-between",
            }}>
                <Toolbar>
                    <Hidden xsDown>
                        <Drawer
                            variant="permanent"
                            open
                        >
                            <div
                                style={{
                                    position: "fixed",
                                    bottom: 0,
                                    top: 0,
                                    background: "#313a46",
                                    border: "none",
                                    boxShadow: "0 0 35px 0 rgba(154,161,171,.15)",
                                }}
                            >
                                <ListSideMenu isLogin={login} />
                            </div>
                        </Drawer>
                    </Hidden>
                    <Hidden smUp>
                        <IconButton
                            // color="inherit"
                            onClick={handleDrawerToggle}
                        >
                            <i className="material-icons">menu</i>
                        </IconButton>
                    </Hidden>
                    <FormGroup>
                        <FormControlLabel
                            style={{ color: "#6c757d" }}
                            labelPlacement="start"
                            control={<Switch checked={login} onChange={handleSwitchToggle} />}
                            label={login ? 'Login' : 'Logout'}
                        />
                    </FormGroup>
                </Toolbar>
                <Hidden smUp>
                    <SwipeableDrawer
                        variant="temporary"
                        anchor={"left"}
                        open={mobileOpen}
                        onClose={handleDrawerToggle}
                    >
                        <div
                            role="presentation"
                            onClick={handleDrawerToggle}
                            onKeyDown={handleDrawerToggle}
                            style={{
                                height: "100%",
                                bottom: 0,
                                top: 0,
                                border: "none",
                                background: "#313a46",
                            }}
                        >
                            <ListSideMenu isLogin={login} />
                        </div>
                    </SwipeableDrawer>
                </Hidden>
            </AppBar>
        </div>
    )
}

export default Header