import React from 'react'

// @material-ui/core components
import Grid from '@material-ui/core/Grid'
import Avatar from '@material-ui/core/Avatar'
import Typography from '@material-ui/core/Typography'

const ProfileBox = (props) => {

    const { name } = props

    return (

        <Grid container spacing={1} wrap="nowrap" justify="center" alignItems="center">
            <Grid item>
                <Avatar style={{ margin: "10px", color: "#313a46", background: "#fafbfe" }}>{name.charAt(0)}</Avatar>
            </Grid>
            <Grid item xs={12} sm container>
                <Grid item xs container direction="column" spacing={1}>
                    <Typography variant="subtitle1">{name}</Typography>
                </Grid>
            </Grid>
        </Grid>
    )
}

export default ProfileBox