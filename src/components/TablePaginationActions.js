import React from 'react'
import PropTypes from 'prop-types'

// @material-ui/core components
import IconButton from '@material-ui/core/IconButton'

const TablePaginationActions = (props) => {
    const { count, page, rowsPerPage, onChangePage } = props

    const handleFirstPageButtonClick = event => {
        onChangePage(event, 0)
    }

    const handleBackButtonClick = event => {
        onChangePage(event, page - 1)
    }

    const handleNextButtonClick = event => {
        onChangePage(event, page + 1)
    }

    const handleLastPageButtonClick = event => {
        onChangePage(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1))
    }

    return (
        <div style={{ flexShrink: "0" }}>
            <IconButton
                onClick={handleFirstPageButtonClick}
                disabled={page === 0}
                aria-label="first page"
            >
                <i className="material-icons">first_page</i>
            </IconButton>
            <IconButton onClick={handleBackButtonClick} disabled={page === 0} aria-label="previous page">
                <i className="material-icons">keyboard_arrow_left</i>
            </IconButton>
            <IconButton
                onClick={handleNextButtonClick}
                disabled={page >= Math.ceil(count / rowsPerPage) - 1}
                aria-label="next page"
            >
                <i className="material-icons">keyboard_arrow_right</i>
            </IconButton>
            <IconButton
                onClick={handleLastPageButtonClick}
                disabled={page >= Math.ceil(count / rowsPerPage) - 1}
                aria-label="last page"
            >
                <i className="material-icons">last_page</i>
            </IconButton>
        </div>
    )
}

export default TablePaginationActions

TablePaginationActions.propTypes = {
    count: PropTypes.number.isRequired,
    onChangePage: PropTypes.func.isRequired,
    page: PropTypes.number.isRequired,
    rowsPerPage: PropTypes.number.isRequired,
}