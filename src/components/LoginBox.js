import React, { useState } from 'react'

// @material-ui/core components
import Container from '@material-ui/core/Container'
import Grid from '@material-ui/core/Grid'
import Card from '@material-ui/core/Card'
import CardHeader from '@material-ui/core/CardHeader'
import CardContent from '@material-ui/core/CardContent'
import Typography from '@material-ui/core/Typography'
import FormControl from '@material-ui/core/FormControl'
import InputLabel from '@material-ui/core/InputLabel'
import OutlinedInput from '@material-ui/core/OutlinedInput'
import InputAdornment from '@material-ui/core/InputAdornment'
import Button from '@material-ui/core/Button'
import IconButton from '@material-ui/core/IconButton'
import Link from '@material-ui/core/Link'


const LoginBox = (props) => {

    const { showReset } = props

    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [showPassword, setShowPassword] = useState(false)

    const handleOnchangeEmail = (event) => {
        setEmail(event.target.value)
    }

    const handleOnChangePassword = (event) => {
        setPassword(event.target.value)
    }

    const handleClickShowPassword = () => {
        setShowPassword(!showPassword)
    }

    const handleOnSubmit = () => {
        console.log(email)
        console.log(password)
    }

    return (
        <Container component="main" maxWidth="sm">

            <Card
                style={{
                    color: "#fff",
                    border: "none",
                    boxShadow: "0 0 35px 0 rgba(154,161,171,.15)",
                    margin: "65px 0px 24px 0px"
                }}
            >
                <CardHeader className="card-header"
                    title={
                        <Typography align="center" variant="h4" style={{ color: "#fff", padding: "10px" }}>Login In</Typography>
                    }
                >
                </CardHeader>
                <CardContent style={{ padding: "55px 35px" }}>
                    <Grid container spacing={2} justify="center" alignContent="center" alignItems="center">

                        <Grid item xs={12} sm={12}>
                            <FormControl variant="outlined" fullWidth margin="dense">
                                <InputLabel>E-mail</InputLabel>
                                <OutlinedInput
                                    type={"text"}
                                    value={email}
                                    onChange={handleOnchangeEmail}
                                    autoFocus={true}
                                    labelWidth={45} />
                            </FormControl>
                        </Grid>

                        <Grid item xs={12} sm={12}>
                            <FormControl variant="outlined" fullWidth margin="dense">
                                <InputLabel>Password</InputLabel>
                                <OutlinedInput
                                    type={showPassword ? "text" : "password"}
                                    value={password}
                                    onChange={handleOnChangePassword}
                                    endAdornment={
                                        <InputAdornment position="end">
                                            <IconButton size="small" onClick={handleClickShowPassword}>
                                                <i className="material-icons-outlined"> {showPassword ? "visibility_off" : "visibility"}</i>
                                            </IconButton>
                                        </InputAdornment>
                                    } labelWidth={70} />
                            </FormControl>
                        </Grid>

                        {showReset &&
                            <Grid item xs={12} sm={12}>
                                <Typography variant="body2" align="right" style={{ margin: "10px" }}><Link href="/teacher/reset-password" underline="none" color="textSecondary">Forgot your password?</Link></Typography>
                            </Grid>
                        }

                        <Grid item xs={12} sm={12}>

                            <Button style={{
                                background: "#727cf5",
                                border: 0,
                                borderRadius: 12,
                                boxShadow: "0 2px 6px 0 rgba(114,124,245,.5)",
                                color: "#fff",
                                height: 40,
                                padding: "0 30px",
                            }}
                                variant="outlined"
                                color="secondary"
                                size="medium"
                                fullWidth
                                onClick={handleOnSubmit}
                            >
                                Log In
                        </Button>

                        </Grid>
                    </Grid>
                </CardContent>
            </Card>
        </Container >
    )
}

LoginBox.defaultProps = {
    showReset: false
}

export default LoginBox