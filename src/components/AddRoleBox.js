import React from 'react'

// @material-ui/core components=
import Typography from '@material-ui/core/Typography'
import Card from '@material-ui/core/Card'
import Grid from '@material-ui/core/Grid'
import FormControl from '@material-ui/core/FormControl'
import InputLabel from '@material-ui/core/InputLabel'
import OutlinedInput from '@material-ui/core/OutlinedInput'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import Checkbox from '@material-ui/core/Checkbox'
import Button from '@material-ui/core/Button'

const AddRoleBox = () => {

    return (
        <React.Fragment>
            <Typography variant="h6" style={{ padding: "15px 0px" }}>Add Role</Typography>
            <Card
                style={{
                    color: "#6c757d",
                    border: "none",
                    boxShadow: "0 0 35px 0 rgba(154,161,171,.15)",
                    padding: "20px"
                }}
            >
                <Typography variant="h4" style={{ padding: "20px 0px" }}>Role Info</Typography>

                <Grid container spacing={2}>
                    <Grid item xs={12} md={8}>
                        <FormControl variant="outlined" fullWidth margin="dense">
                            <InputLabel>Name</InputLabel>
                            <OutlinedInput
                                type={"text"}
                                autoFocus={true}
                                labelWidth={45} />
                        </FormControl>
                    </Grid>

                    <Grid item xs={12} md={8}>
                        <FormControl variant="outlined" fullWidth margin="dense">
                            <InputLabel>Remark</InputLabel>
                            <OutlinedInput
                                multiline
                                rows="5"
                                type={"text"}
                                labelWidth={55} />
                        </FormControl>
                    </Grid>

                    <Grid item xs={12} md={12}>
                        <Typography variant="subtitle1" display="inline">Status : </Typography>
                        <FormControlLabel style={{ padding: "5px", color: "#6c757d" }}
                            control={<Checkbox color="primary" />}
                            label="Active"
                        />
                    </Grid>

                    <Grid
                        container
                        direction="row"
                        justify="flex-end"
                        alignItems="center"
                        style={{ padding: "10px 10px" }}
                    >
                        <Button
                            variant="contained"
                            size="small"
                            style={{
                                backgroundColor: "#0acf97",
                                border: 0,
                                borderColor: "#0acf97",
                                boxShadow: "0 2px 6px 0 rgba(10,207,151,.5)",
                                color: "#fff",
                                padding: "7px 15px",
                            }}
                        >
                            Create Role
                        </Button>
                    </Grid>
                </Grid>
            </Card>
        </React.Fragment>
    )
}

export default AddRoleBox