import React, { useState } from 'react'

// @material-ui/core components=
import Typography from '@material-ui/core/Typography'
import Card from '@material-ui/core/Card'
import Grid from '@material-ui/core/Grid'
import FormControl from '@material-ui/core/FormControl'
import InputLabel from '@material-ui/core/InputLabel'
import OutlinedInput from '@material-ui/core/OutlinedInput'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import Checkbox from '@material-ui/core/Checkbox'
import Button from '@material-ui/core/Button'

const AddStaffBox = () => {

    const [state, setState] = useState({
        firstname: "",
        lastname: "",
        email: "",
        password: "",
        confirmPassword: "",
        active: false
    })

    const handleOnchangeFirstName = (event) => {
        setState({ ...state, firstname: event.target.value })
    }

    const handleOnchangeLastName = (event) => {
        setState({ ...state, lastname: event.target.value })
    }

    const handleOnchangeEmail = (event) => {
        setState({ ...state, email: event.target.value })
    }

    const handleOnchangePassword = (event) => {
        setState({ ...state, password: event.target.value })
    }

    const handleOnchangeConfirmPassword = (event) => {
        setState({ ...state, confirmPassword: event.target.value })
    }

    const handleOnChangeActive = (event) => {
        setState({ ...state, active: event.target.checked })
    }

    const handleOnSubmit = () => {
        console.log(state.firstname)
        console.log(state.lastname)
        console.log(state.email)
        console.log(state.password)
        console.log(state.confirmPassword)
        console.log(state.active)
    }

    return (
        <React.Fragment>
            <Typography variant="h6" style={{ padding: "15px 0px" }}>Add Staff</Typography>
            <Card
                style={{
                    color: "#6c757d",
                    border: "none",
                    boxShadow: "0 0 35px 0 rgba(154,161,171,.15)",
                    padding: "20px"
                }}
            >
                <Typography variant="h4" style={{ padding: "20px 0px" }}>Staff Info</Typography>

                <Grid container spacing={2}>
                    <Grid item xs={12} md={6}>
                        <FormControl variant="outlined" fullWidth margin="dense">
                            <InputLabel>Firstname</InputLabel>
                            <OutlinedInput
                                type={"text"}
                                value={state.firstname}
                                onChange={handleOnchangeFirstName}
                                autoFocus={true}
                                labelWidth={75} />
                        </FormControl>
                    </Grid>

                    <Grid item xs={12} md={6}>
                        <FormControl variant="outlined" fullWidth margin="dense">
                            <InputLabel>Lastname</InputLabel>
                            <OutlinedInput
                                type={"text"}
                                value={state.lastname}
                                onChange={handleOnchangeLastName}
                                labelWidth={70} />
                        </FormControl>
                    </Grid>

                    <Grid item xs={12} md={8}>
                        <FormControl variant="outlined" fullWidth margin="dense">
                            <InputLabel>E-mail</InputLabel>
                            <OutlinedInput
                                type={"text"}
                                value={state.email}
                                onChange={handleOnchangeEmail}
                                labelWidth={45} />
                        </FormControl>
                    </Grid>

                    <Grid item xs={12} md={6}>
                        <FormControl variant="outlined" fullWidth margin="dense">
                            <InputLabel>Password</InputLabel>
                            <OutlinedInput
                                type={"text"}
                                value={state.password}
                                onChange={handleOnchangePassword}
                                labelWidth={70} />
                        </FormControl>
                    </Grid>

                    <Grid item xs={12} md={6}>
                        <FormControl variant="outlined" fullWidth margin="dense">
                            <InputLabel>Password Confirm</InputLabel>
                            <OutlinedInput
                                type={"text"}
                                value={state.confirmPassword}
                                onChange={handleOnchangeConfirmPassword}
                                labelWidth={135} />
                        </FormControl>
                    </Grid>

                    <Grid item xs={12} md={4}>
                        <Typography variant="subtitle1" display="inline">Status : </Typography>
                        <FormControlLabel style={{ padding: "5px", color: "#6c757d" }}
                            control={<Checkbox color="primary" checked={state.active} onChange={handleOnChangeActive} value={state.active} />}
                            label="Active"
                        />
                    </Grid>

                    <Grid
                        container
                        direction="row"
                        justify="flex-end"
                        alignItems="center"
                        style={{ padding: "10px 10px" }}
                    >
                        <Button
                            variant="contained"
                            size="small"
                            style={{
                                backgroundColor: "#0acf97",
                                border: 0,
                                borderColor: "#0acf97",
                                boxShadow: "0 2px 6px 0 rgba(10,207,151,.5)",
                                color: "#fff",
                                padding: "7px 15px",
                            }}
                            onClick={handleOnSubmit}
                        >
                            Create Staff
                        </Button>
                    </Grid>
                </Grid>
            </Card>
        </React.Fragment>
    )
}

export default AddStaffBox