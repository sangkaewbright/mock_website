import React from 'react'
import DateFnsUtils from '@date-io/date-fns'
import { MuiPickersUtilsProvider, KeyboardDatePicker } from '@material-ui/pickers'

// @material-ui/core components
import Typography from '@material-ui/core/Typography'
import Card from '@material-ui/core/Card'
import Grid from '@material-ui/core/Grid'
import FormControl from '@material-ui/core/FormControl'
import InputLabel from '@material-ui/core/InputLabel'
import OutlinedInput from '@material-ui/core/OutlinedInput'
import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'

const HistoryUpdate = (props) => {

    const { title } = props

    return (
        <React.Fragment>
            <Typography variant="h6" style={{ padding: "15px 0px" }}>{"Update " + title}</Typography>
            <Card
                style={{
                    color: "#6c757d",
                    border: "none",
                    boxShadow: "0 0 35px 0 rgba(154,161,171,.15)",
                    padding: "20px"
                }}
            >
                <Typography variant="h4" style={{ padding: "20px 0px" }}>{title + " Info"}</Typography>

                <Grid container spacing={2}>
                    <Grid item xs={12} md={6}>
                        <FormControl variant="outlined" fullWidth margin="dense">
                            <InputLabel>Grade</InputLabel>
                            <OutlinedInput
                                type={"text"}
                                autoFocus={true}
                                labelWidth={45} />
                        </FormControl>
                    </Grid>

                    <Grid item xs={12} md={6}>
                        <FormControl variant="outlined" fullWidth margin="dense">
                            <InputLabel>Room</InputLabel>
                            <OutlinedInput
                                type={"text"}
                                labelWidth={45} />
                        </FormControl>
                    </Grid>

                    <Grid item xs={12} md={2}>
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <KeyboardDatePicker
                                disableToolbar
                                variant="outlined"
                                format="MM/dd/yyyy"
                                margin="normal"
                                id="date-picker-inline"
                                label="DateTime"
                            />
                        </MuiPickersUtilsProvider>
                    </Grid>

                    <Grid item xs={12} md={2}>
                        <TextField
                            id="outlined-number"
                            label="Attend"
                            type="number"
                            inputProps={{
                                step: 1,
                                min: 0,
                                max: 100,
                                type: 'number'
                            }}
                            margin="normal"
                            variant="outlined"
                            style={{ width: 150 }}
                        />
                    </Grid>

                    <Grid item xs={12} md={2}>
                        <TextField
                            id="outlined-number"
                            label="Absent"
                            type="number"
                            inputProps={{
                                step: 1,
                                min: 0,
                                max: 100,
                                type: 'number'
                            }}
                            margin="normal"
                            variant="outlined"
                            style={{ width: 150 }}
                        />
                    </Grid>

                    <Grid
                        container
                        direction="row"
                        justify="flex-end"
                        alignItems="center"
                        style={{ padding: "10px 10px" }}
                    >
                        <Button
                            variant="contained"
                            size="small"
                            style={{
                                backgroundColor: "#0acf97",
                                border: 0,
                                borderColor: "#0acf97",
                                boxShadow: "0 2px 6px 0 rgba(10,207,151,.5)",
                                color: "#fff",
                                padding: "7px 15px",
                            }}
                        >
                            Update Section
                        </Button>
                    </Grid>
                </Grid>
            </Card>
        </React.Fragment>
    )
}

export default HistoryUpdate