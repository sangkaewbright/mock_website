import React from 'react'

// @material-ui/core components
import List from '@material-ui/core/List'
import ListItem from "@material-ui/core/ListItem"
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import Divider from '@material-ui/core/Divider'

// components
import ProfileBox from './ProfileBox'

const loginList = [
    { link: "/staff/login", icon: "lock", label: "Staff Login" },
    { link: "/teacher/login", icon: "lock", label: "Teacher Login" },
]

const list = [
    // { link: "/staff/login", icon: "lock", label: "Staff Login" },
    // { link: "/teacher/login", icon: "lock", label: "Teacher Login" },
    { link: "/staff-list", icon: "view_list", label: "Staff List" },
    { link: "/school-list", icon: "view_list", label: "School List" },
    { link: "/role-list", icon: "view_list", label: "Role List" },
    { link: "/section-list", icon: "view_list", label: "Section List" },
    { link: "/teacher-list", icon: "view_list", label: "Teacher List" },
    { link: "/student-list", icon: "view_list", label: "Student List" },
    { link: "/attend-history", icon: "history", label: "Attend History" },
    { link: "/lunch-history", icon: "history", label: "Lunch History" },
    { link: "/sleep-history", icon: "history", label: "Sleep History" },
    { link: "/logout", icon: "exit_to_app", label: "Logout" },
]

const ListSideMenu = (props) => {

    const { isLogin } = props

    return (
        <div
            style={{
                position: "relative",
                display: "flex",
                width: 250,
                height: "100%",
                color: "#8391a2",
                background: "#313a46",
                overflowY: "auto",
            }}
        >

            <List
                style={{
                    width: "100%",
                    margin: 0,
                    paddingLeft: "0",
                    listStyle: "none",
                    paddingTop: "0",
                    paddingBottom: "0",
                    background: "#313a46",
                }}
            >

                <ListItem style={{ padding: "15px 10px", color: "#fff" }}>
                    {
                        isLogin ?
                            <ProfileBox name={"Alex Henry"} />
                            :
                            <div style={{ margin: "10px auto" }} />
                    }
                </ListItem>

                <Divider />

                {!isLogin ?

                    loginList.map((item, i) => (
                        <ListItem key={i} button component="a" href={item.link} style={{ padding: "15px 30px" }}>
                            <ListItemIcon style={{ color: "#8391a2" }}><i className="material-icons-outlined">{item.icon}</i></ListItemIcon>
                            <ListItemText primary={`${item.label}`} />
                        </ListItem>
                    ))

                    :
                    list.map((item, i) => (
                        <ListItem key={i} button component="a" href={item.link} style={{ padding: "15px 30px" }}>
                            <ListItemIcon style={{ color: "#8391a2" }}><i className="material-icons-outlined">{item.icon}</i></ListItemIcon>
                            <ListItemText primary={`${item.label}`} />
                        </ListItem>
                    ))
                }

            </List>
        </div>
    )
}

ListSideMenu.defaultProps = {
    isLogin: false
}

export default ListSideMenu