import React, { useState } from 'react'

// @material-ui/core components
import Container from '@material-ui/core/Container'
import Grid from '@material-ui/core/Grid'
import Card from '@material-ui/core/Card'
import CardHeader from '@material-ui/core/CardHeader'
import CardContent from '@material-ui/core/CardContent'
import Typography from '@material-ui/core/Typography'
import FormControl from '@material-ui/core/FormControl'
import InputLabel from '@material-ui/core/InputLabel'
import OutlinedInput from '@material-ui/core/OutlinedInput'
import InputAdornment from '@material-ui/core/InputAdornment'
import Button from '@material-ui/core/Button'
import IconButton from '@material-ui/core/IconButton'


const ResetPasswordBox = () => {

    const [email, setEmail] = useState('')

    const handleOnchangeEmail = (event) => {
        setEmail(event.target.value)
    }

    const handleOnSubmit = () => {
        console.log(email)
    }

    const handleOnClear = () => {
        setEmail('')
    }


    return (
        <Container component="main" maxWidth="sm">
            <Card
                style={{
                    color: "#fff",
                    border: "none",
                    boxShadow: "0 0 35px 0 rgba(154,161,171,.15)",
                    margin: "65px 0px 24px 0px"
                }}
            >
                <CardHeader className="card-header"
                    title={
                        <Typography align="center" variant="h4" style={{ color: "#fff", padding: "10px" }}>Reset Password</Typography>
                    }
                >
                </CardHeader>
                <CardContent style={{ padding: "30px 35px 55px 35px" }}>
                    <Grid container spacing={2} justify="center" alignContent="center" alignItems="center">

                        <Grid item xs={12} sm={12}>
                            <Typography style={{ color: "#6c757d", padding: "15px" }} align="center" variant="subtitle1">
                                Enter your email address and we'll send you an email with instructions to reset your password.
                            </Typography>
                        </Grid>

                        <Grid item xs={12} sm={12}>
                            <FormControl variant="outlined" fullWidth margin="dense">
                                <InputLabel>E-mail</InputLabel>
                                <OutlinedInput
                                    type={"text"}
                                    value={email}
                                    onChange={handleOnchangeEmail}
                                    autoFocus={true}
                                    endAdornment={
                                        <InputAdornment position="end">
                                            <IconButton size="small" onClick={handleOnClear}>
                                                <i className="material-icons">clear</i>
                                            </IconButton>
                                        </InputAdornment>
                                    } labelWidth={45} />
                            </FormControl>
                        </Grid>

                        <div style={{ margin: "15px 0px 0px 0px", padding: "2px" }} />

                        <Grid item xs={12} sm={12}>
                            <Button style={{
                                background: "#727cf5",
                                border: 0,
                                borderRadius: 12,
                                boxShadow: "0 2px 6px 0 rgba(114,124,245,.5)",
                                color: "#fff",
                                height: 40,
                                padding: "0 30px",
                            }}
                                variant="outlined"
                                color="secondary"
                                size="medium"
                                fullWidth
                                onClick={handleOnSubmit}
                            >
                                Reset Password
                        </Button>
                        </Grid>

                    </Grid>
                </CardContent>
            </Card>
        </Container>
    )
}

export default ResetPasswordBox