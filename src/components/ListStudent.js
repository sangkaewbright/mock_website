import React from 'react'

// @material-ui/core components
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid'
import Card from '@material-ui/core/Card'
import Button from '@material-ui/core/Button'
import Table from '@material-ui/core/Table'
import TableHead from '@material-ui/core/TableHead'
import TableBody from '@material-ui/core/TableBody'
import TableFooter from '@material-ui/core/TableFooter'
import TableRow from '@material-ui/core/TableRow'
import TableCell from '@material-ui/core/TableCell'
import IconButton from '@material-ui/core/IconButton'
import Avatar from '@material-ui/core/Avatar'
import Tooltip from '@material-ui/core/Tooltip'
import TablePagination from '@material-ui/core/TablePagination'

// components
import TablePaginationActions from './TablePaginationActions'

const list = [
    { id: 54, grade: 3.5, room: 205, firstname: "Hayward", lastname: "Billo", nickname: "Bill", gender: "male", email: "haywardbillo@gmail.com", contactNumber: "0846548178", point: 10, status: true },
    { id: 75, grade: 2.1, room: 302, firstname: "Adler", lastname: "Villa", nickname: "Vill", gender: "male", email: "adlervilla@gmail.com", contactNumber: "0846548178", point: 35, status: false },
    { id: 45, grade: 1.5, room: 452, firstname: "Janna", lastname: "Gardner", nickname: "Gar", gender: "male", email: "jannamacFayden@gmail.com", contactNumber: "0846548178", point: 85, status: true },
    { id: 86, grade: 2.0, room: 302, firstname: "Patel", lastname: "Lane", nickname: "Lan", gender: "male", email: "peradi7327@4tmail.net", contactNumber: "0846548178", point: 87, status: false },
    { id: 78, grade: 3.99, room: 302, firstname: "Burns", lastname: "Gonzales", nickname: "Gon", gender: "female", email: "wendycollins25@gmail.com", contactNumber: "0846548178", point: 65, status: false },
    { id: 21, grade: 4.0, room: 205, firstname: "Edwards", lastname: "Barnes", nickname: "Bar", gender: "female", email: "wendycollins25@gmail.com", contactNumber: "0846548178", point: 54, status: true },
    { id: 68, grade: 3.85, room: 111, firstname: "Kelly", lastname: "Payne", nickname: "Pay", gender: "female", email: "wendycollins25@gmail.com", contactNumber: "0846548178", point: 75, status: true },
    { id: 45, grade: 2.25, room: 111, firstname: "Ellis", lastname: "Johnston", nickname: "Jhon", gender: "female", email: "matthewscott58@gmail.com", contactNumber: "0846548178", point: 89, status: true },
    { id: 86, grade: 1.58, room: 222, firstname: "Gibson", lastname: "King", nickname: "King", gender: "female", email: "haywardbillo@gmail.com", contactNumber: "0846548178", point: 10, status: true },
    { id: 86, grade: 2.15, room: 235, firstname: "Brown", lastname: "Sullivan", nickname: "Suli", gender: "female", email: "gavinjohnson47@gmail.com", contactNumber: "0846548178", point: 99, status: true },
    { id: 86, grade: 3.58, room: 302, firstname: "Rodriguez", lastname: "Cruz", nickname: "Cruz", gender: "female", email: "timothyeady18@gmail.com", contactNumber: "0846548178", point: 78, status: true },
    { id: 86, grade: 4.0, room: 302, firstname: "Mills", lastname: "Arnold", nickname: "Arno", gender: "female", email: "stellahill61@gmail.com", contactNumber: "0846548178", point: 35, status: true },
    { id: 86, grade: 4.0, room: 555, firstname: "Wood", lastname: "Richardson", nickname: "Richard", gender: "female", email: "jeramiahvasko21@gmail.com", contactNumber: "0846548178", point: 100, status: true },
    { id: 86, grade: 4.0, room: 555, firstname: "Palmer", lastname: "Reed", nickname: "Re", gender: "male", email: "michaelgagnon84@gmail.com", contactNumber: "0846548178", point: 25, status: true },
]

const ListStudent = () => {

    const [page, setPage] = React.useState(0)
    const [rowsPerPage, setRowsPerPage] = React.useState(5)

    const handleChangePage = (event, newPage) => {
        setPage(newPage)
    }

    const handleChangeRowsPerPage = event => {
        setRowsPerPage(parseInt(event.target.value, 10))
        setPage(0)
    }

    return (
        <React.Fragment>
            <Typography variant="h6" style={{ padding: "15px 0px" }}>Student List</Typography>
            <Card
                style={{
                    color: "#fff",
                    border: "none",
                    boxShadow: "0 0 35px 0 rgba(154,161,171,.15)",
                    padding: "20px",
                    overflowX: "auto"
                }}
            >
                <Grid container>
                    <Grid container justify="flex-start">
                        <Button
                            component="a" href={"/student-add"}
                            startIcon={<i className="material-icons-outlined">add_circle_outline</i>}
                            variant="contained"
                            size="small"
                            style={{
                                background: "#0acf97",
                                border: 0,
                                borderColor: "#0acf97",
                                boxShadow: "0 2px 6px 0 rgba(10,207,151,.5)",
                                color: "#fff",
                                padding: "7px 15px",
                            }}
                        >

                            Add Student
                        </Button>
                    </Grid>
                    <Grid container alignContent="center">
                        <Table style={{ margin: "35px 0px 25px 0px", color: "#6c757d" }}>
                            <TableHead style={{ background: "#f1f3fa" }}>
                                <TableRow>
                                    <TableCell>#</TableCell>
                                    <TableCell align="left">Avatar</TableCell>
                                    <TableCell align="left">Grade</TableCell>
                                    <TableCell align="left">Room</TableCell>
                                    <TableCell align="left">Firstname</TableCell>
                                    <TableCell align="center">Lastname</TableCell>
                                    <TableCell align="center">Nickname</TableCell>
                                    <TableCell align="center">Gender</TableCell>
                                    <TableCell align="center">E-mail</TableCell>
                                    <TableCell align="center">Contact Number</TableCell>
                                    <TableCell align="center">Point</TableCell>
                                    <TableCell align="center">Status</TableCell>
                                    <TableCell align="center">Action</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {(rowsPerPage > 0
                                    ? list.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                    : list
                                ).map((item) => (
                                    <TableRow key={item.firstname}>
                                        <TableCell>{item.id}</TableCell>
                                        <TableCell align="left">
                                            <Avatar>{item.firstname.charAt(0)}</Avatar>
                                        </TableCell>
                                        <TableCell align="left">{item.grade}</TableCell>
                                        <TableCell align="left">{item.room}</TableCell>
                                        <TableCell align="left">{item.firstname}</TableCell>
                                        <TableCell align="left">{item.lastname}</TableCell>
                                        <TableCell align="left">{item.nickname}</TableCell>
                                        <TableCell align="left">{item.gender}</TableCell>
                                        <TableCell align="left">{item.email}</TableCell>
                                        <TableCell align="left">{item.contactNumber}</TableCell>
                                        <TableCell align="left">
                                            {item.status ?
                                                <span className="badge badge-success">Active</span>
                                                :
                                                <span className="badge badge-danger">Deactivated</span>
                                            }
                                        </TableCell>
                                        <TableCell align="left">{item.point}</TableCell>
                                        <TableCell align="center">
                                            <Tooltip title="Edit" placement="top">
                                                <IconButton size="small">
                                                    <i className="material-icons-outlined md-18">edit</i>
                                                </IconButton>
                                            </Tooltip>
                                            <Tooltip title="Delete" placement="top">
                                                <IconButton size="small">
                                                    <i className="material-icons-outlined md-18">delete</i>
                                                </IconButton>
                                            </Tooltip>
                                        </TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                            <TableFooter>
                                <TableRow>
                                    <TablePagination
                                        rowsPerPageOptions={[5, 10, 25]}
                                        colSpan={13}
                                        count={list.length}
                                        rowsPerPage={rowsPerPage}
                                        page={page}
                                        onChangePage={handleChangePage}
                                        onChangeRowsPerPage={handleChangeRowsPerPage}
                                        ActionsComponent={TablePaginationActions}
                                    />
                                </TableRow>
                            </TableFooter>
                        </Table>
                    </Grid>
                </Grid>
            </Card>
        </React.Fragment>
    )
}

export default ListStudent