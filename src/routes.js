import React from 'react'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'

// import Page
import LoginStaffPage from '../src/pages/LoginStaffPage'
import LoginTeacherPage from '../src/pages/LoginTeacherPage'
import UpdateAttendPage from './pages/UpdateAttendPage'
import UpdateLunchPage from './pages/UpdateLunchPage'
import UpdateSleepPage from './pages/UpdateSleepPage'

//import Component
import ResetPasswordBox from '../src/components/ResetPasswordBox'

// List
import ListStaff from './components/ListStaff'
import ListTeacher from './components/ListTeacher'
import ListStudent from './components/ListStudent'
import ListSchool from './components/ListSchool'
import ListRole from './components/ListRole'
import ListSection from './components/ListSection'

// Add
import AddStaffBox from './components/AddStaffBox'
import AddTeacherBox from './components/AddTeacherBox'
import AddStudentBox from './components/AddStudentBox'
import AddSchoolBox from './components/AddSchoolBox'
import AddRoleBox from './components/AddRoleBox'
import AddSectionBox from './components/AddSectionBox'

// History
import HistoryAttend from './components/HistoryAttend'
import HistoryLunch from './components/HistoryLunch'
import HistorySleep from './components/HistorySleep'


const Routes = () => (
    <Router>
        <Switch>
            {/* ------------------------------ Login -------------------------------- */}
            <Route exact path="/staff/login" component={LoginStaffPage} />
            <Route exact path="/teacher/login" component={LoginTeacherPage} />
            <Route exact path="/teacher/reset-password" component={ResetPasswordBox} />

            {/* ------------------------------ List -------------------------------- */}
            <Route exact path="/staff-list" component={ListStaff} />
            <Route exact path="/teacher-list" component={ListTeacher} />
            <Route exact path="/student-list" component={ListStudent} />
            <Route exact path="/school-list" component={ListSchool} />
            <Route exact path="/role-list" component={ListRole} />
            <Route exact path="/section-list" component={ListSection} />

            {/* ------------------------------ Add -------------------------------- */}
            <Route exact path="/staff-add" component={AddStaffBox} />
            <Route exact path="/teacher-add" component={AddTeacherBox} />
            <Route exact path="/student-add" component={AddStudentBox} />
            <Route exact path="/school-add" component={AddSchoolBox} />
            <Route exact path="/role-add" component={AddRoleBox} />
            <Route exact path="/section-add" component={AddSectionBox} />

            {/* ------------------------------ history -------------------------------- */}
            <Route exact path="/attend-history" component={HistoryAttend} />
            <Route exact path="/lunch-history" component={HistoryLunch} />
            <Route exact path="/sleep-history" component={HistorySleep} />

            {/* ------------------------------ update -------------------------------- */}
            <Route exact path="/attend-update" component={UpdateAttendPage} />
            <Route exact path="/lunch-update" component={UpdateLunchPage} />
            <Route exact path="/sleep-update" component={UpdateSleepPage} />

            {/* ------------------------------ Notfound -------------------------------- */}
            <Route component={null} />
        </Switch>
    </Router>
)

export default Routes