import React from 'react'

// import Component
import HistoryUpdate from '../components/HistoryUpdate'

const UpdateLunchPage = () => (
    <HistoryUpdate title="Lunch" />
)

export default UpdateLunchPage