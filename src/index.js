import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { BrowserRouter } from 'react-router-dom'

import App from './App';

const AppWithRouter = () => (
    <BrowserRouter>
        <App />
    </BrowserRouter>
)

ReactDOM.render(<AppWithRouter />, document.getElementById('root'));